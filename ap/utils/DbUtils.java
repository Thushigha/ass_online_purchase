package ap.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtils {

	private static final String USER = "root";// Database user name
	private static final String UPWD = "root";// Database password
	// Local database shop
	private static final String URL = "jdbc:mysql://localhost:3306/shop";
	// drive
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	// Registration driven
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Get the function of database Connection object Connection
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USER, UPWD);
	}

	// Close connected and executed open resources
	public static void close(Connection connection, Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// Close all open resources
	public static void close(Connection connection, Statement statement, ResultSet rs) {
		if (statement != statement) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
