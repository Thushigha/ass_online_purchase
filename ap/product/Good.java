package ap.product;

public class Good {

	private int id;

	private String name;

	private float price;

	private int num;

	public Good() {
		super();
	}

	@Override
	public String toString() {
		return "Good [id=" + id + ", name=" + name + ", price=" + price + ", num=" + num + "]";
	}

	public Good(int id, String i, float price, int num) {
		super();
		this.id = id;
		this.name = i;
		this.price = price;
		this.num = num;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getName() {

		return 0;
	}

}
